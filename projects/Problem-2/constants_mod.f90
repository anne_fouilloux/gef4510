module constants_mod
  implicit none
  integer, parameter :: N = 1201
  integer, parameter :: Jmax = 27

  real, parameter    :: PI = 4.0 * atan(1.0)

  real, parameter :: D = 100
  real, parameter    :: DeltaZ = D/(Jmax-1)
  real, parameter   :: theta0=10.0

contains

subroutine write_constants()
  implicit none

  print*, 'N = ', N
  print*, 'Jmax = ', Jmax
  print*, 'PI = ', PI
  print*, 'D = ', D
  print*, 'DeltaZ = ', deltaZ
  print*, 'thetat0 = ', theta0
end subroutine write_constants
end module constants_mod
