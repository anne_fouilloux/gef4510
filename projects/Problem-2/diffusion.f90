PROGRAM diffusion
  use constants_mod
  implicit none

  real, dimension(Jmax,N) :: theta
  real, dimension(Jmax)   :: z

  integer                 :: i

  call write_constants()
  call init(theta)
!  do i=1,Jmax
!    print*, z(i), theta(i,1)
!  end do

  do i = 1, N-1
    theta(:,i+1) = next_theta(z,theta(:, i))
    print*, 'theta next ', theta(1,i+1)
  enddo

 
contains
!------------------
subroutine init(t)
  implicit none
  real, dimension(:,:), intent(out) :: t
  integer :: i

  z(1) = 0.0
  do i=1,Jmax-1
    z(i+1) = z(i) + DeltaZ
  end do
  t(:,1) = theta0 * sin(PI*z/D)

end subroutine init
!-----------------------
function next_theta(z,t)
  implicit none
  real, dimension(:), intent(in) :: t
  real, dimension(:), intent(in) :: z

  real, dimension(Jmax) :: next_theta

! compute boundary conditions
  next_theta(1) = 0
  next_theta(Jmax) = 0

! compute next theta
 
end function next_theta

END PROGRAM diffusion
