program truncationErrors
  use constants_mod
  use truncationErrors_mod
  implicit none

  type(Z_t)       :: Z1, Z
  integer         :: i

  print*, 'PI= ', PI

  call init(Z1, Z)

  do i=1,1000
     call nextSub(Z,Z1)
     call write(Z)
  end do

 call finalize(Z)
end program truncationErrors
