module truncationErrors_mod
  implicit none

  type Z_t
    real :: val
  end type Z_t
contains
!----------------------------------
type(Z_t) function next(Z,Z1)
  use constants_mod
  implicit none
  type(Z_t), intent(in) :: Z, Z1

  next%val = a * Z%val - b * Z1%val
  
end function next
!----------------------------------
subroutine nextSub(ZZ,ZZ1)
 use constants_mod
 implicit none
  type(Z_t), intent(in):: ZZ1
  type(Z_t), intent(inout) :: ZZ

  ZZ%val = a * ZZ%val - b * ZZ1%val
end subroutine nextSub
!----------------------------------
subroutine init(ZZ1,ZZ)
 use constants_mod
 implicit none
  type(Z_t), intent(out) :: ZZ1
  type(Z_t), intent(out) :: ZZ
  ZZ1%val=PI
  ZZ = ZZ1

  open(unit=10, file='result.txt', access='sequential', &
       form='formatted', position='rewind')
end subroutine init
!----------------------------------
subroutine finalize(ZZ)
 implicit none
  type(Z_t), intent(out) :: ZZ

  close(unit=10)
end subroutine finalize
!----------------------------------
subroutine write(ZZ)
 implicit none
 type(Z_t), intent(in) :: ZZ

 write(10,*) 'Z = ', ZZ
end subroutine write
end module truncationErrors_mod
