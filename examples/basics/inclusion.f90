PROGRAM inclusion
  IMPLICIT NONE
  INTEGER :: i
! to make your "big" code easier to read 
! you can use some INCLUDE
! Please note that we usually prefer
! to create separate Fortran module instead
  INCLUDE 'inclusion.inc'

  DO i=1,6
     rtab(i) = PI
  END DO
  ltab(1) = .true.
  ltab(2) = .false.
  ltab(3) = .true.
  CALL sub
END PROGRAM inclusion
!
SUBROUTINE sub
  IMPLICIT NONE
  INCLUDE 'inclusion.inc'

  PRINT*,'rtab   = ', rtab
  PRINT*,'ltab   = ', ltab
END SUBROUTINE sub
