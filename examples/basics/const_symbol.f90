PROGRAM const_symbol
  implicit none

  logical, parameter :: ISTRUE = .TRUE., ISFALSE = .FALSE.
  double precision   :: PI, RTOD
! not that we use d0 when specifying a double precision constant
  parameter             (PI=3.15159265d0, RTOD=180.d0/PI)
  
  print*, 'ISTRUE = ', ISTRUE
  print*, 'ISFALSE = ', ISFALSE

  print*, 'PI = ', PI
  print*, 'RTOD = ', RTOD

END PROGRAM const_symbol
