program selectcase
  implicit none

 integer :: month, nb_of_days, year
 logical :: leap_year

 print*, 'Enter a year'
 read*, year
 print*, 'Enter a month (1-12)'
 read*, month

 if( ((year/4*4     .eq. year) .and. &
      (year/100*100 .ne. year)) .or. &
      (year/400*400 .eq. year) ) then
   leap_year = .true.
 else
   leap_year = .false.
 end if

 select case(month)
   case(4,6,9,11) 
     nb_of_days = 30
   case(1,3,5,7:8,10,12)
     nb_of_days = 31
   case(2)
!----------------------------------
     february: select case(leap_year)
       case(.true.)
         nb_of_days = 29
       case(.false.)
         nb_of_days = 28
     end select february
!-----------------------------------
   case DEFAULT 
     print*, 'Invalid month (1-12)'
 end select 

 print*, "There are ", nb_of_days, " days in this month."

end program selectcase
