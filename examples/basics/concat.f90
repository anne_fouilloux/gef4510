PROGRAM concat
  implicit none

  character(len=10) :: ch ='Hel'

  print*, "-",ch,"-"
 
! ch = ch // 'llo' --> does not work!

! You need to remove blanks (10-3 blanks) at the
! end of the string using TRIM function:
 
  ch = TRIM(ch) // 'lo'

  print*, "-",ch,"-"

END PROGRAM concat
