PROGRAM priority

  implicit none
  real    :: a, b, c, d
  LOGICAL :: e, f, g

!
! arithmetic expression
!
  print*, "  2**3**2           = ", 2**3**2
  print*, "  2**(3**2)         = ", 2**(3**2)
  print*, "  5. + 4.*9.**2     = ", 5. + 4.*9.**2
  print*, "  5. + (4.*(9.**2)) = ", 5. + (4.*(9.**2))


!
! Logical expressions 
!
  a = 3.
  b = 5.
  c = 6.
  d = 678.

  e = .TRUE.
  f = .FALSE.
  g = .TRUE.

  print*, "e.OR.f.AND.g            = ", e.OR.f.AND.g
  print*, "e.OR.(f.AND.g)          = ", e.OR.(f.AND.g)
  print*, "a**b+c.GT.d.AND.e       = ", a**b+c.GT.d.AND.e
  print*, "(((a**b)+c).GT.d).AND.e = ", (((a**b)+c).GT.d).AND.e

END PROGRAM priority
