PROGRAM construct_vector

 IMPLICIT NONE
 integer i

! Initialisation via le constructuer de vecteur
! Initialization of a vector via an array constructor

 real,             dimension(4)  :: heights = (/ 5.1, 5.6, 4.0, 3.6 /)
 character(len=6), dimension(3)  :: colours = (/ 'RED  ', 'GREEN', 'BLUE ' /)
 integer,          dimension(10) :: ints    = (/ 100, (i, i=1,8), 100 /)

 print*, "heights = ", heights
 print*, "colours = ", colours
 print*, "ints    = ", ints

END PROGRAM construct_vector
