PROGRAM conv_implicit
  implicit none

   integer :: a,b,c

   a = 99
   b = 100
   print*, "99/100 ", a/b

   a = 7
   b = 3
   print*, "7/3    ", a/b


   a = 100
   b = 9
   c = 5
   print*, "(100*9)/5", (a*b)/c


   a = 9
   b = 5
   c = 100
   print*, "(9/5)*100", (a/b)*c

END PROGRAM conv_implicit
