PROGRAM affect
  implicit none

   real    :: x
   integer :: n, m

!
! Initialization
!
   x = 5
   n = 0.9999
   m = -1.9999

!
! print outputs
!
  print*, "Expression        Interpretation"
  print*, "  x = 5               x = ", x
  print*, "  n = 0.9999          n = ", n
  print*, "  m = -1.9999         m = ", m

END PROGRAM affect
