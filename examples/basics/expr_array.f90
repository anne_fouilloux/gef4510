PROGRAM expr_array

 IMPLICIT NONE
 real, dimension(-4:0,0:2) :: b
 real, dimension(5,3)      :: c
 real, dimension(0:4,0:2)  :: d
 
 b = 3.
 c = 5.
 d = 7.

 b = c * b - b**2
 print*, b
 print*
 b = sin(c) + cos(d)
 print*, b

! sometimes the following notation is use too
! i.e. it shows the number of dimensions of
! each array and may avoid errors in your array expressions
!
 b(:,:) = 3.
 c(:,:) = 5.
 d(:,:) = 7.

 b(:,:) = c(:,:) * b(:,:) - b(:,:)**2
 print*, b(:,:)
 print*
 b(:,:) = sin(c(:,:)) + cos(d(:,:))
 print*, b(:,:)

END PROGRAM expr_array
