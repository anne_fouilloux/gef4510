PROGRAM if_statement
  implicit none

  real a, b, min, hrs
  real sum

  print*, ' Enter a real a '
  read*, a
  print*, ' Enter a real b '
  read*, b

  sum =0.
  if ( a .lt. b ) then
    min = a
    print*, 'min = ', min
  end if

  print*, ' Enter a real HRS '
  read*, hrs 
  if ( hrs .le. 40. ) then
    print*, 'hrs <= 40.'
    a = hrs*150.
  else if ( hrs .le. 50. ) then
    print*, '40. < hrs <= 50.'
    a = (hrs-40.)*150.*1.5
  else
    print*, 'hrs > 50.'
    a = (hrs-50.)*150.0*2.0
  end if

  print*, 'a = ', a

END PROGRAM if_statement
