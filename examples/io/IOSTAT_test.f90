program read_file
  implicit none
  integer date
  integer ios

  open( UNIT=1, FILE="IOSTAT_test.in", action="read" )

! loop where we use exit if ios is not 0 (0 means no errors)
  do
! FMT is I4 which means it must read a 4 digits integer

    read( UNIT=1, FMT='(I4)', IOSTAT=ios ) date
    if ( ios /= 0 ) EXIT
    print *, date
  end do
  close( UNIT=1 )
  if ( ios < 0 ) print *, "End of file"
  if ( ios > 0 ) print *, "An error has been detected!"
end program read_file
