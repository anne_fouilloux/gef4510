program sequential_write
! using binary file is more efficient than ASCII (formatted)
! files but it is less portable
! Well know IO libraries (netCDF, HDF, etc.) must be 
! preferred when writing large output files
!
  implicit none
  integer, parameter   :: NA=100
  real, dimension(NA)  :: tab
  integer              :: i = 200
  integer              :: j
  real                 :: r 
  integer              :: ios
  
  
  r = acos(-1.)
  open( unit=1,                         &
        file = "sequential.in", &
        form="unformatted",             &
        access = "sequential",          &
        action="write",                  &
        iostat=ios )
  
  if ( ios .ne. 0 ) then
    print*," Error: cannot open output file"
  else
    do j=1,NA
      tab(j) = j
    end do
    write( unit=1 ) tab, i, r
  end if
  
  close( unit=1 )

end program sequential_write
