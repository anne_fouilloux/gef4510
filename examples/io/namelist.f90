program namelist
! namelist can be useful to specify
! a set of input parameters to your code
! (instead of having a long list of arguments
! in your code)
  implicit none
  integer ios
  integer i/100/, &
          j/200/, &
          k/300/
  integer t(3)
  character(len=11) ch
  NAMELIST/MYLIST/ i, j, k, t, ch
  
  
  open( unit=1,                &
        file="namelist.in",    &
        form="formatted",      &
        access="sequential",   &
        status="old",          &
        action="read",         &
        position="rewind",     &
        iostat=ios )
  
  open( unit=2,                &
        file="namelist.out",   &
        form="formatted",      &
        access="sequential",   &
        status="new",          &
        action="write",        &
        position="rewind",     &
        iostat=ios )
  
! read a namelist: you only need to 
! specify the namelist name (here mylist)
  read ( unit=1, nml=mylist )
! same as read. You can write into a namelist
! very easily
  write( unit=2, nml=mylist )
  
  close( unit=1 )
  close( unit=2 )

end program namelist
