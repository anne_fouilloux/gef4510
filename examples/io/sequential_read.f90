program sequential_read
! using binary file is more efficient than ASCII (formatted)
! files but it is less portable
! Well know IO libraries (netCDF, HDF, etc.) must be 
! preferred when writing large output files
!
  implicit none
  real, dimension(100) :: tab
  integer              :: i = 100
  real                 :: r 
  integer              :: ios
  
  
  r = acos(-1.)
  open( unit=1,                         &
        file = "sequential.in", &
        form="unformatted",             &
        access = "sequential",          &
        action="read",                  &
        position="rewind",              &
        iostat=ios )
  
  if ( ios .ne. 0 ) then
    print*," Error: cannot open input file"
  else
    read( unit=1 ) tab, i, r
    print*, tab(1:100:49), i, r
  end if
  
  close( unit=1 )

end program sequential_read
