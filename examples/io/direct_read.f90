program direct_access

  implicit none
  integer, parameter   :: NA=100
  real, dimension(NA)  :: tab
  integer              :: ios, n

  OPEN( UNIT=1,                    &
        FILE="direct.in", &
        ACCESS="direct",           &
        ACTION="read",             &
        STATUS="old",              &
        RECL=400,                  &
        IOSTAT=ios )

  if ( ios /= 0 ) then 
     print*, "Cannot open file direct.in"
  else 

    print*,'Which record do you want to read?'
    read*, n
    read( unit=1, rec=n, iostat=ios ) tab
    if ( ios > 0 ) then 
      print '(a,i0,/,a)', "Error when reading &
                         & record ", n, &
                       "    ==> Are you sure this record exists?"
    else
      print*, tab(1:100:49)
    end if
  end if
  close( unit=1 )

end program direct_access
