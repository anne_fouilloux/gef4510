program read_file
  implicit none
  integer date

  open( UNIT=1, FILE="END_file.in", action="read" )
  do
    read( UNIT=1, FMT='(I4)', END=1 ) date
    print *, date
  end do
1 close( UNIT=1 )
end program read_file
