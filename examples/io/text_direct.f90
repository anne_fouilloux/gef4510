program text_direct

  implicit none
  real, dimension(100) :: tab
  integer              :: ios, n

  OPEN( UNIT=1,                  &
        FILE="text_direct.in",   &
        ACCESS="direct",         &
        FORM="formatted",        &
        ACTION="read",           &
        STATUS="old",            &
        RECL=800,                &
        IOSTAT=ios )

  if ( ios /= 0 ) then 
    print*, "Can't open text_direct.in"
  else 
    print*,'Which record do you want to read: '
    read( unit=*, fmt=* ) n
    read( unit=1, rec=n, fmt='(100F8.4)', iostat=ios ) tab
    if ( ios > 0 ) then 
      print '(a,i0,/,a)', "Error when reading &
                         & record ", n, &
                          "    ==> does it really exist?"
    else
      print '(F8.4)', tab(1:100:49)
    end if
  end if
  close( unit=1 )

end program text_direct
