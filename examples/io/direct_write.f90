program direct_access

  implicit none
  integer, parameter   :: NA=100
  integer, parameter   :: Nrec=200
  real, dimension(NA)  :: tab
  integer              :: ios, n

  OPEN( UNIT=1,                    &
        FILE="direct.in",          &
        ACCESS="direct",           &
        ACTION="write",            &
        STATUS="old",              &
        RECL=400,                  &
        IOSTAT=ios )

  if ( ios /= 0 ) then 
     print*, "Cannot open file direct.in"
  else 
    do n=1,NA
      tab(n) = n
    end do
    do n=1,Nrec
      write( unit=1, rec=n, iostat=ios ) tab
      if ( ios > 0 ) then 
        print '(a,i0,/,a)', "Error when writing &
                           & record ", n
      end if
      tab(:) = tab(:) + 1
    end do
  end if
  close( unit=1 )

end program direct_access
