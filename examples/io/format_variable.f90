program format_variable

  implicit none
  integer, parameter   :: n=4, m=6
  real, dimension(n,m) :: tab 
  character(len=8)     :: fmt = "(  F8.3)"
  integer              :: i,j,ios

  write(fmt(2:3), '(i2)') n
  tab(1:n:2,:) = acos(-1.)
  tab(2:n:2,:) = exp(1.)
  open( unit=1,                     &
        file="format_variable.out", &
        position="rewind",          &
        action="write",             &
        status="new",               &
        iostat=ios )

  if ( ios /= 0 ) then 
    print*, "Can't open file"
  else
    write( unit=1, fmt=fmt ) ( (tab(i,j), i=1,n), j=1,m )
  end if
     
  close( unit=1 )

end program format_variable
