program arg_tab
  implicit none
  integer , parameter    :: n = 3, m = 2
  real , dimension (n,m) :: tab
  real                   :: sum_value

  read *,tab; 
  print *, sum_value ( tab , n, m )
end program arg_tab

real function sum_value ( t, n, m )
  implicit none
  integer :: n,m,i,j
  real , dimension (n,m) :: t

  sum_value = 0.

  do i=1,n
    do j=1,m
     sum_value = sum_value + t(i,j)
    end do
  end do
end function sum_value
