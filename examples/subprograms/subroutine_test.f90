
program test
  implicit none

  REAL , DIMENSION (100) :: tab
  REAL                   :: avg , maximum
  integer                :: i

  do i=1,100
    tab(i) = i*0.05
  enddo

  CALL SP( tab , avg , maximum )

  PRINT *, avg , maximum
end program test

SUBROUTINE SP( t, avg , max )
  implicit none
  REAL , DIMENSION (100) :: t
  REAL :: avg , max
  INTEGER :: i
 
  max = t (1); avg = t (1)
  DO i =2 ,100
    IF ( t(i) > max ) max = t(i)
    avg = avg + t(i)
  END DO

  avg = avg /100.0
END SUBROUTINE SP
