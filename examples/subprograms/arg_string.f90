! this program demonstrates how to pass string arguments
program arg_string
  implicit none
  character ( len =10) :: ch

  read '(a)',ch
  call conv ( ch )
  print *,ch
end program arg_string
! conv convert a lower case string to an upper case string
subroutine conv ( chaine )
  implicit none
  character ( len =*) :: chaine
  integer i, j

  do i=1, len ( chaine )
    if( iachar ( chaine (i:i) ) < 97 .or. &
      iachar ( chaine (i:i) ) > 122 ) cycle

     j = iachar ( chaine (i:i) ) - 32
     chaine (i:i) = achar ( j )
  end do
end subroutine conv
