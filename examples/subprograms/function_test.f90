program test
  implicit none
  REAL , DIMENSION (100) :: tab
  REAL                   :: avg , maximum
  REAL                   :: maxi
  integer                :: i

  do i=1,100
    tab(i) = i* 0.05
  enddo

  maximum = maxi ( tab , avg )
  PRINT *, avg , maximum

end program test

FUNCTION maxi ( t, avg )
  implicit none
  REAL , DIMENSION (100) :: t
  REAL :: avg , maxi
  INTEGER :: i

  maxi = t (1); avg = t(1)
  DO i =2 ,100
    IF ( t(i) > maxi ) maxi = t(i)
    avg = avg + t(i)
  END DO

  avg = avg /100.0
END FUNCTION maxi
