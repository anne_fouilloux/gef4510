MODULE parkind
  implicit none

  integer, parameter   :: i4=SELECTED_INT_KIND(8)
  integer, parameter   :: i8=SELECTED_INT_KIND(12)
  integer, parameter   :: r4=SELECTED_REAL_KIND(6,37)
  integer, parameter   :: r8=SELECTED_REAL_KIND(15,307)

END MODULE parkind
