MODULE constantVariables
  USE parkind
  implicit none

  real(kind=r8), parameter :: g=9.80616    ! Acceleration due to gravity
  real(kind=r8), parameter :: R=6.37122d06 ! Radius of earth
  
END MODULE constantVariables
