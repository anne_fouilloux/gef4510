program test
  USE parkind
  USE constantVariables
  USE MODEL

  implicit none

  TYPE(state) :: M

  print*, 'Acceleration due to gravity', g
  print*, 'Radius of erath ', R

  call init_model(M)

  call print_state(M)

end program test
