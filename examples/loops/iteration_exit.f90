PROGRAM iteration_exit
 implicit none

  real            :: value
  real            :: x, xlast
  real, parameter :: tolerance = 1.0e-6

  print*, 'Enter a real value ' 
  read*, value

  x = 1.0        ! initial value 
  do
   xlast = x
   x = 0.5 * (xlast + value/xlast)
   if ( abs( x-xlast )/x < tolerance ) exit
  end do
  print*, "The square root of ", value, " is ", x

END PROGRAM iteration_exit

