PROGRAM iteration_while
 implicit none

  integer          :: n
  double precision :: sum_value, epsilon 
  logical          :: finished

  print*, " Enter a value for epsilon (< 0.01)"
  read*, epsilon

! initialization

  n = 1
  sum_value = 0
  finished = .false.
  do while ( .not. finished )
    sum_value = sum_value + 1d0/n**2
    n = n + 1
    finished = (1d0/n**2 .lt. epsilon*sum_value)
  end do

  print*, "Number of iterations : ", n

END PROGRAM iteration_while
