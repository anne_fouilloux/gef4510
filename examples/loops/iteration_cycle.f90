program iteration_cycle
 implicit none

 integer :: year

 do
   print*, 'Enter a year (-1 to exit the loop)'
   read*, year
! If you enter -1, you exit the loop
   if ( year .le. 0 ) exit
! if the following statement is true, 
!  the CYCLE statement brings the control back 
!  to the beginning of the loop to
!  read a new value for year
   if( ((year/4*4     .eq. year) .and. &
        (year/100*100 .ne. year)) .or. &
        (year/400*400 .eq. year) ) cycle
! When the previous if statement is false,
! it comes here 
   print*, year, " is not a leap year"
 end do

end program iteration_cycle
