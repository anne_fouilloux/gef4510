PROGRAM iteration_do

  implicit none

  integer i, sum_value, n

  print*, 'Enter an integer  n '
  read*, n

  sum_value = 0

! do loop with start=1, end=n and step=2
  do i=1,n,2
    sum_value = sum_value + i
  end do

  print*, 'The sum is ', sum_value

END PROGRAM iteration_do
