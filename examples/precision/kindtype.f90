program kindtype
!
!
! Demonstrates the use of kind values.
!
!

implicit none

integer, parameter   :: i2=SELECTED_INT_KIND(4)  ! integer between 10^-4 and 10^4
integer, parameter   :: i4=SELECTED_INT_KIND(8)  ! integer between 10^-8 and 10^8
integer, parameter   :: i8=SELECTED_INT_KIND(12) ! integer between 10^-12 and 10^12
integer, parameter   :: r4=SELECTED_REAL_KIND(6,37)
integer, parameter   :: r8=SELECTED_REAL_KIND(15,307)
integer, parameter   :: r16=SELECTED_REAL_KIND(33,4931)
integer(KIND=i2) :: ia
integer(KIND=i4) :: ib
integer(KIND=i8) :: ic
real(KIND=r4) :: ra
real(KIND=r8) :: rb
real(KIND=r16) :: rc

print *, 'Kind for 2-bytes integer'
print *, '   SELECTED_INT_KIND(4) is            ', i2
print *, '   Number of significant digits       ', DIGITS(ia)         
print *, '   Largest number                     ', HUGE(ia)       
print *, '   Base of the model                  ', RADIX(ia)      
print *, '   Decimal exponent range             ', RANGE(ia)      

print *, 'Kind for 4-bytes integer'
print *, '   SELECTED_INT_KIND(8) is            ', i4
print *, '   Number of significant digits       ', DIGITS(ib)         
print *, '   Largest number                     ', HUGE(ib)       
print *, '   Base of the model                  ', RADIX(ib)      
print *, '   Decimal exponent range             ', RANGE(ib)      

print *, 'Kind for 8-bytes integer'
print *, '   SELECTED_INT_KIND(12) is           ', i8
print *, '   Number of significant digits       ', DIGITS(ic)         
print *, '   Largest number                     ', HUGE(ic)       
print *, '   Base of the model                  ', RADIX(ic)      
print *, '   Decimal exponent range             ', RANGE(ic)      

print *, 'Kind for single real precision'
print *, '   SELECTED_REAL_KIND(6,37) is        ', r4
print *, '   Number of significant digits       ', DIGITS(ra)         
print *, '   Almost negligible compared to one  ', EPSILON(ra)    
print *, '   Largest number                     ', HUGE(ra)       
print *, '   Maximum model exponent             ', MAXEXPONENT(ra) 
print *, '   Minimum model exponent             ', MINEXPONENT(ra) 
print *, '   Decimal precision                  ', PRECISION(ra)  
print *, '   Base of the model                  ', RADIX(ra)      
print *, '   Decimal exponent range             ', RANGE(ra)      
print *, '   Smallest positive number           ', TINY(ra)

print *, 'Kind for double real precision'
print *, '   SELECTED_REAL_KIND(15,307) is      ', r8
print *, '   Number of significant digits       ', DIGITS(rb)         
print *, '   Almost negligible compared to one  ', EPSILON(rb)    
print *, '   Largest number                     ', HUGE(rb)       
print *, '   Maximum model exponent             ', MAXEXPONENT(rb) 
print *, '   Minimum model exponent             ', MINEXPONENT(rb) 
print *, '   Decimal precision                  ', PRECISION(rb)  
print *, '   Base of the model                  ', RADIX(rb)      
print *, '   Decimal exponent range             ', RANGE(rb)      
print *, '   Smallest positive number           ', TINY(rb)

print *, 'Kind for quadruple real precision'
print *, '   SELECTED_REAL_KIND(33,4931) is     ', r16
print *, '   Number of significant digits       ', DIGITS(rc)         
print *, '   Almost negligible compared to one  ', EPSILON(rc)    
print *, '   Largest number                     ', HUGE(rc)       
print *, '   Maximum model exponent             ', MAXEXPONENT(rc) 
print *, '   Minimum model exponent             ', MINEXPONENT(rc) 
print *, '   Decimal precision                  ', PRECISION(rc)  
print *, '   Base of the model                  ', RADIX(rc)      
print *, '   Decimal exponent range             ', RANGE(rc)      
print *, '   Smallest positive number           ', TINY(rc)
stop
end program kindtype
