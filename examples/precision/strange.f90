program strange
  implicit none

! It is important to choose the right type when defining your variables.
! some numbers cannot be represented; See example below.

  integer :: myint = 33554431 
  real :: myreal = 33554431 
  real(kind=8) :: mydouble = 33554431 


  print*, 'myint = ', myint
  print*, 'myreal = ', myreal
  print*, 'mydouble = ', mydouble
end program strange
