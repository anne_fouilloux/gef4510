program derived_type
  implicit none

  type state
    real, dimension(10) :: T, U, V
  end type state

  type(state) :: M

  call init_model(M)
  call print_state(M)

  contains

  subroutine init_model(Model)
    implicit none
    type(state), intent(out) :: Model
    integer :: i

    do i=1,size(Model%T)
       Model%T(i)=273.6+0.001*i
       Model%U(i)=4.5+0.1*i
       Model%V(i)=Model%U(i)/2.0
    enddo
  end subroutine init_model
  subroutine print_state(Model)
    implicit none
    type(state), intent(in) :: Model
    integer :: i
  
    do i=1,size(Model%T)
      print*, 'T(',i,')=',Model%T(i)
      print*, 'U(',i,')=',Model%U(i)
      print*, 'V(',i,')=',Model%V(i)
    enddo
  end subroutine print_state
end program derived_type
