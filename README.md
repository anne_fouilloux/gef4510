# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains Fortran examples or exercises to be done to learn Fortran programming.

### How do I get set up? ###

All examples need to be compiled/linked with a Fortran compiler.

On our machines at UIO, GNU compilers and intel compilers are both available.

All exercices/examples have been tested and run on sverdrup.uio.no

If you login from a linux machine, you can use ssh to login to sverdrup:

ssh -Y sverdrup.uio.no

From Windows use X-Win32 or Putty (http://www.putty.org/) or any other ssh client.
From Mac you may use "terminal" or XQuartz (http://xquartz.macosforge.org/landing/).
Use your UiO account name and password to log in to sverdrup.uio.no from any machine.

Every Fortran code needs to be compiled before you can execute it. 

To compile a Fortran code with GNU compilers:

*gfortran -o test.x test.f90*

where test.f90 is your FORTRAN code.
 

To run your code called test.x

*./test.x*

On sverdrup, it is wise to use a recent GNU compiler and as it is not available by default, you need to set up your environment:

*module load gcc/4.9.0*

it makes both C/C++ and Fortran GNU compilers (version 4.9.0) available to you. Then proceed as usual.

### How to get all the examples on my computer? ###

After you login on sverdrup (in a X-term):

*mkdir Fortran*

*cd Fortran*

*git clone git@bitbucket.org:anne_fouilloux/gef4510.git*

A new directory called gef4510 will appear:

*ls*

*gef4510*


(on sverdrup, git is available but if of another machine, you get "command not found", you need to install git software).

### How do I install a Fortran compiler on my personal computer? ###

You need at least two things to be able to develop and run Fortran codes on your own computer:

* an editor (emacs, vim, nedit, etc.) to create or edit Fortran codes.
* a Fortran compiler (GNU compilers are free and available both for linux and Windows)

## Windows ##

If you have little experience with software installation I suggest you install:

* emacs (http://ftp.gnu.org/pub/gnu/emacs/windows/) for editing your Fortran programs.
Download emacs-24.2-bin-i386.zip  and  install it on your computer.

* To install GNU compilers, take http://tdm-gcc.tdragon.net/
For Windows 64 bits, choose TDM64 bundle (36.4MB - GCC 4.8.1)

"MinGW Command Prompt" is what you need to execute to get some kind of terminal. gfortran command line is available from this terminal.

## Linux ##

Depending on your linux distribution (red hat, Ubuntu, etc.) precompiled version of both emacs and GNU compilers are available.

* emacs http://www.gnu.org/software/emacs/
* GNU compilers https://gcc.gnu.org/wiki/GFortran


## Fortran free compilers ##

For a more exhaustive list of free Fortran compilers, I suggest you check http://www.thefreecountry.com/compilers/fortran.shtml

## Fortran materials ##

* Gunnar Wollan has compiled a set of documents at http://folk.uio.no/gunnarw/GEO4060/ and you can also find some of his documents at http://www.uio.no/studier/emner/matnat/geofag/GEF4510/h12/index.html

* An interesting free tutorial at http://www.fortrantutorial.com/
  You can skip the installation of silverfrost compilers if you already have a Fortran compiler installed on your machine or if you are using sverdrup.uio.no

* The National Computational Infrastructure in Australia has class notes online for a Basic Fortran course (http://nf.nci.org.au/training/FortranBasic/) and an Advanced Fortran course (http://nf.nci.org.au/training/FortranAdvanced/). 

* Fortran Wiki http://fortranwiki.org/fortran/show/HomePage

* Fortran GEO4060 UIO course at http://annefou.github.io/Fortran/

### Who do I talk to? ###

* Anne Fouilloux (anne.fouilloux-at-geo.uio.no)
