!//////////////////////////////////////////////////////////////////////////
!//
!// This is Problem set 1 "Truncation error" of GEF4510 fall 2013
!//-----------------------------------------------------------------------
!// Purpose:
!//   - To compute the mathematical number pi from two different recursion
!//     series. The objective is to show that the series diverges, and 
!//     that it is the initial truncation error that makes it diverge 
!//     from pi.
!//-----------------------------------------------------------------------
!// Method:
!//   The recursion formulaes or series are:
!//     
!//      Z_{n+1} = 3.1*Z_{n} - 2.1*Z_{1}
!//      S_{n+1} = (9.0/5.0)*S_{n} - (4.0/5.0)*S_{1}
!//
!//   We are required to print out a nice heading, then the resulting
!//   Z_{n+1}, S_{n+1} and the relative errors for n=1(1)100. The 
!//   relative errors are given by:
!//
!//      EZ_{n} = (Z_{n+1} - Z_{n})/Z_{1} 
!//      ES_{n} = (S_{n+1} - S_{n})/Z_{1} 
!// 
!//-----------------------------------------------------------------------
!// Revision history:
!//  1980/81      : Code originally written in Fortran 77 by Lars Petter 
!//                 R�ed while visiting Florida State University.
!//  Nov 11, 2010 : Restructered the program and added more comments 
!//                 and explanations. Still Fortran 77
!//  Sep 01, 2013 : Rewritten into Fortran 95
!//  Sep 11, 2013 : Debugged for errors, specifically in recursion formulaes
!//  
!//////////////////////////////////////////////////////////////////////////
!
!
PROGRAM trunc 
  IMPLICIT NONE
!//-----------------------------------------------------------------------
! to get the expected precision
! uncomment/ comment if you want to try different type of real
! SP: single precision
! DP: double precision
! QP: quadruple precision
!  integer, parameter              :: myreal=SELECTED_REAL_KIND(6,37)    ! SP
!  integer, parameter              :: myreal=SELECTED_REAL_KIND(15,307)  ! DP
  integer, parameter              :: myreal=SELECTED_REAL_KIND(33,4931) ! QP
!// Declare variables
  INTEGER,PARAMETER               :: NT=2       ! Dimension of arrays
  REAL(KIND=myreal),DIMENSION(NT)    :: Z          ! Variable of first recursion series 
                                               ! (Double precision)
  REAL(KIND=myreal),DIMENSION(NT)    :: S          ! Variable of second recursion series
  REAL(KIND=myreal)                  :: PERZ       ! Relative error in Z
  REAL(KIND=myreal)                  :: PERS       ! Relative error in S
  INTEGER                            :: res        ! Error variable for in and out files
!//-----------------------------------------------------------------------
!// Declare indexes
  INTEGER              :: N          ! Loop counter
  INTEGER              :: NOLD       ! Holds previous value of variable
  INTEGER              :: NNEW       ! Holds new value of variable
  INTEGER              :: NSAVE      ! Auxiliary integer used when swapping
                                     ! indexes
  INTEGER,PARAMETER    :: NMAX=1000  ! Maximum number if iterations
  INTEGER,PARAMETER    :: NWR=1      ! Number of iterations between writing
                                     ! results to file
!//-----------------------------------------------------------------------
!// Declare constants (here as single precision; try double precision and see
!   what happens!
  REAL                 :: PI,AZ,BZ,AS,BS
!//-----------------------------------------------------------------------
!// Declare character strings to hold the filenames
  CHARACTER(LEN=80)      :: outfile 
!// Declare constant values for the Logical Unit Number (olun) for
!// referencing the file to which the results are written
  INTEGER,PARAMETER      :: olun = 11
!//-----------------------------------------------------------------------
!// Assign values to constants

  PI=4._myreal*ATAN(1._myreal)                 ! Mathematical constant = 3.1415...       
  AZ=3.1_myreal                            ! 1st coefficient in the Z series
  BZ=2.1_myreal                            ! 2nd coefficient in the Z series
  AS=9._myreal/5._myreal                       ! 1st coefficient in the S series
  BS=4._myreal/5._myreal                       ! 2nd coefficient in the S series
!// Assign an output filename to hold the results
  outfile = "trunc.txt"
!//-----------------------------------------------------------------------
!// Read in coefficients from terminal
!// (To activate erase !// in the 3 first columns)
!//       WRITE(6,*) 'Read in the coefficients in the formulas for Z and S'
!//       WRITE(6,*) 'There are four of them (AZ,BZ,AS,BS)'
!//       WRITE(6,*) ' '
!//       READ(5,*) AZ,BZ,AS,BS
!//-----------------------------------------------------------------------
!// Initialize counters and variables
  N=0
  NOLD=1
  NNEW=2
  NSAVE=0
  Z(NOLD)=PI
  S(NOLD)=PI
  PERZ=0.
  PERS=0.
!//-----------------------------------------------------------------------
!// Write PI and coefficents to terminal
  PRINT *, 'PI=',PI,'AZ=',AZ,' BZ=',BZ,' AS=',AS,' BS=',BS
!
!// Open the outfile
  OPEN(UNIT=olun,FILE=outfile,FORM="FORMATTED",IOSTAT=res)
!// Test if the opening of the file was successful
  IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
    PRINT *, "Error in opening output file, status: ", res
!// Stop the program
    STOP
  END IF       
!
!// Write a heading to the outfile
  WRITE(UNIT=olun,FMT='(7X,A1,8X,A4,12X,A7,9X,A4,12X,A7)',IOSTAT=res) &
   'N','Z(N)','ERROR Z','S(N)','ERROR S'
!// Test if the operation was successful
  IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
    PRINT *, "Error in writing file, status: ", res
!// Stop the program
    STOP
  END IF
  WRITE(UNIT=olun,FMT='(2X,A72)',IOSTAT=res) &
  '------------------------------------------------------------------------'
!// Test if the operation was successful
  IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
    PRINT *, "Error in writing file, status: ", res
!// Stop the program
    STOP
  END IF
!// Write the inital values of the variables to the outfile
    WRITE(UNIT=olun,FMT='(3X,I5,6X,E10.4,6X,E10.4,6X,E10.4,6X,E10.4)',IOSTAT=res) &
    N,Z(NOLD),PERZ,S(NOLD),PERS
!// Test if the operation was successful
  IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
    PRINT *, "Error in writing file, status: ", res
!// Stop the program
    STOP
  END IF
!//-----------------------------------------------------------------------
!// Start the iteration (recursion)
  DO N=1,NMAX        
!//-----------------------------------------------------------------------
!// Compute pi using the two recursion formulas
    Z(NNEW)=AZ*Z(NOLD)-BZ*PI
    S(NNEW)=AS*S(NOLD)-BS*PI
!//-----------------------------------------------------------------------
!// Compute the relative error in percent
    PERZ=100.*(Z(NNEW)-Z(NOLD))/Z(NOLD)
    PERS=100.*(S(NNEW)-S(NOLD))/S(NOLD)
!//-----------------------------------------------------------------------
!// Should we write the results to the outfile
    IF(MOD(N,NWR).EQ.0) THEN
!// Write the results to the outfile
      WRITE(UNIT=olun,FMT='(3X,I5,6X,E10.4,6X,E10.4,6X,E10.4,6X,E10.4)',IOSTAT=res) &
      N,Z(NNEW),PERZ,S(NNEW),PERS
!// Test if the operation was successful
      IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
        PRINT *, "Error in writing file, status: ", res
!// Exit loop
        EXIT
      END IF
    END IF
!//-----------------------------------------------------------------------
!// Swap the indices
    NSAVE=NOLD
    NOLD=NNEW
    NNEW=NSAVE
!//-----------------------------------------------------------------------
!// Do next iteration step
  END DO
!// Close the output file
  CLOSE(UNIT=olun)
!//------------------------------------------------------------------------
!//                                       End program in an orderly fashion
  PRINT *, ' Stop after successful run'
  STOP
END PROGRAM trunc
