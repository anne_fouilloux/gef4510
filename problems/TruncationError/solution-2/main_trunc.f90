!//////////////////////////////////////////////////////////////////////////
!//
!// This is Problem set 1 "Truncation error" of GEF4510 fall 2013
!//-----------------------------------------------------------------------
!// Purpose:
!//   - To compute the mathematical number pi from two different recursion
!//     series. The objective is to show that the series diverges, and 
!//     that it is the initial truncation error that makes it diverge 
!//     from pi.
!//-----------------------------------------------------------------------
!// Method:
!//   The recursion formulaes or series are:
!//     
!//      Z_{n+1} = 3.1*Z_{n} - 2.1*Z_{1}
!//      S_{n+1} = (9.0/5.0)*S_{n} - (4.0/5.0)*S_{1}
!//
!//   We are required to print out a nice heading, then the resulting
!//   Z_{n+1}, S_{n+1} and the relative errors for n=1(1)100. The 
!//   relative errors are given by:
!//
!//      EZ_{n} = (Z_{n+1} - Z_{n})/Z_{1} 
!//      ES_{n} = (S_{n+1} - S_{n})/Z_{1} 
!// 
!//-----------------------------------------------------------------------
!// Revision history:
!//  1980/81      : Code originally written in Fortran 77 by Lars Petter 
!//                 Røed while visiting Florida State University.
!//  Nov 11, 2010 : Restructered the program and added more comments 
!//                 and explanations. Still Fortran 77
!//  Sep 01, 2013 : Rewritten into Fortran 95
!//  Sep 11, 2013 : Debugged for errors, specifically in recursion formulaes
!//  
!//////////////////////////////////////////////////////////////////////////
!

PROGRAM main_trunc
  USE parkind_mod
  USE constants_mod
  USE series_mod

  IMPLICIT NONE

  TYPE(series)         :: Z  ! Variable of first recursion series
  TYPE(series)         :: S  ! Variable of second recursion series

!// Declare indexes
  INTEGER              :: N                   ! Loop counter
  INTEGER              :: NMAX                ! Maximum number if iterations (read from NAMELIST)
  INTEGER              :: NWR                 ! Number of iterations between writing
                                               ! results to file (read from namelist)
  INTEGER, parameter   :: olun=11    ! Logical Unit Number (olun) for
                                              ! referencing the file to which the results are written
  
  CHARACTER(LEN=80)    :: outfile             ! Declare character strings to hold the filenames
  CHARACTER(LEN=80)    :: namelistfile        ! Declare character strings to hold namelist filename

  INTEGER              :: ios                 ! check return code when opening

  !// A command line argument counter
  !// An input argument counter
  INTEGER              :: argc

  NAMELIST/MODEL_INPUT/ outfile, NMAX, NWR

!============================================================
 !// Retrieve the number of command line arguments
  argc = COMMAND_ARGUMENT_COUNT()
  !// Test if we have  1 argument (namelist filename)
  IF (argc < 1) THEN
    !// Error, print a usage message and stop the program
    PRINT *, 'USAGE: trunc namelist.in'
    STOP
  END IF

  !// Retrieve the first argument and put the contents into
  !// the farenheit_file variable
  CALL GET_COMMAND_ARGUMENT(1,namelistfile)

! read namelist
  open( unit=1,              &
        file=namelistfile,     &
        form="formatted",      &
        iostat=ios )
  if (ios /= 0) then
    print*, 'Cannot open namelist input file ', trim(namelistfile)
    STOP
  endif
! read a namelist
  read ( unit=1, nml=MODEL_INPUT )
!
  close( unit=1 )

! Initialization of Z and S
  call series_init(Z, PI, AZ, BZ)
  call series_init(S, PI, AS, BS)
! print initial values
  N=0
  call series_print(outfile, olun, Z, S, N, NWR)
  
!// Write PI and coefficents to terminal
  PRINT *, 'PI=',PI,'AZ=',Z%coef1,' BZ=',Z%coef2,' AS=',S%coef1,' BS=',S%coef2

!============================================================
  DO N=1,NMAX
   CALL series_compute_next(Z)
   CALL series_compute_next(S)

   CALL series_print(outfile, olun, Z, S, N, NWR)
  END DO

!============================================================
! Finalize code
  CALL series_finalize(olun)
  
END PROGRAM main_trunc
