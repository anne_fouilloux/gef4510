MODULE constants_mod
  USE parkind_mod

  IMPLICIT NONE

  real(kind=myreal), parameter   :: PI = 4.*ATAN(1.) ! Mathematical constant = 3.1415...       
  real(kind=myreal), parameter   :: AZ=3.1           ! 1st coefficient in the Z series
  real(kind=myreal), parameter   :: BZ=2.1           ! 2nd coefficient in the Z series
  real(kind=myreal), parameter   :: AS=9./5.         ! 1st coefficient in the S series
  real(kind=myreal), parameter   :: BS=4./5.         ! 2nd coefficient in the S series

END MODULE constants_mod
