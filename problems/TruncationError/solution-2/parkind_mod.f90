MODULE parkind_mod
! here we define single and double real precision 
! comment/uncomment depending on whether you want single, double or quadruple
! precision
  integer, parameter   :: myreal=SELECTED_REAL_KIND(6,37)
!  integer, parameter   :: myreal=SELECTED_REAL_KIND(15,307)
!  integer, parameter   :: myreal=SELECTED_REAL_KIND(33,4931)
END MODULE parkind_mod
