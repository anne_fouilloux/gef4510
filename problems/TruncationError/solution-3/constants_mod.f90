MODULE constants_mod
  USE parkind_mod

  IMPLICIT NONE

  real(kind=myreal), parameter   :: PI = 4.*ATAN(1.) ! Mathematical constant = 3.1415...       

END MODULE constants_mod
