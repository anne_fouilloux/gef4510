MODULE series_mod
  USE parkind_mod

  IMPLICIT NONE

  TYPE series
    real(kind=myreal) :: initial
    real(kind=myreal) :: current
    real(kind=myreal) :: next
    real(kind=myreal) :: relative_error
    real(kind=myreal) :: coef1, coef2
  END TYPE series

  CONTAINS

!====================================================================
  SUBROUTINE series_init(S, initial_value, coef1_value, coef2_value)
    USE parkind_mod
    IMPLICIT NONE
    type(series), intent(out) :: S
    real(kind=myreal), intent(in) :: initial_value
    real(kind=myreal), intent(in) :: coef1_value, coef2_value

    S%initial        = initial_value
    S%coef1          = coef1_value
    S%coef2          = coef2_value
    S%relative_error = 0.0d0
    S%current        = S%initial
  END SUBROUTINE series_init

!====================================================================
  SUBROUTINE series_compute_next(S)
    USE parkind_mod
    IMPLICIT NONE
    type(series), intent(inout) :: S


!// Compute next value
    S%next = S%coef1 * S%current - S%coef2 * S%initial

!// Compute the relative error in percent
    S%relative_error = 100.*(S%next-S%current)/S%current

    S%current = S%next

  END SUBROUTINE series_compute_next

!====================================================================
  SUBROUTINE series_print(filename, uid,S, niter, nwr)
    USE parkind_mod
    IMPLICIT NONE
    character(len=*), intent(in)    :: filename
    type(series),     intent(in)    :: S
    integer,          intent(in)    :: uid
    integer,          intent(in)    :: niter
    integer,          intent(in)    :: nwr

    integer                         :: res
    logical                         :: OK

    INQUIRE( UNIT=uid, OPENED=OK ) 
    
    IF ( .not. OK ) THEN
! we need to open the output file and write a small header
      call series_print_init(filename,uid)
    ENDIF
! the file is now opened: we write output values
    if (mod(niter,nwr) .eq.0) then
      WRITE(UNIT=uid,FMT='(3X,I5,6X,E10.4,6X,E10.4)',IOSTAT=res) &
      niter,S%current,S%relative_error
    
!// Test if the operation was successful
      IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
        PRINT *, "Error in writing file, status: ", res
!// Stop the program
        STOP
      END IF
    endif
  END SUBROUTINE series_print


!====================================================================
  SUBROUTINE series_print_init(filename,uid)
    USE parkind_mod
    character(len=*), intent(in)    :: filename
    integer,          intent(in)    :: uid
    
    integer(kind=myreal)                :: res

    !// Open the outfile
    OPEN(UNIT=uid,FILE=filename,FORM="FORMATTED",IOSTAT=res)
!// Test if the opening of the file was successful
    IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
      PRINT *, "Error in opening output file, status: ", res
!// Stop the program
      STOP
   END IF  
   WRITE(UNIT=uid,FMT='(7X,A1,8X,A4,12X,A7)',IOSTAT=res) &
        'N','Z(N)','ERROR Z'

    WRITE(UNIT=uid,FMT='(2X,A72)',IOSTAT=res) &
    '------------------------------------------------------------------------'
!// Test if the operation was successful
    IF(res /= 0) THEN
!// No, an error occurred, print a message to the terminal window
      PRINT *, "Error in writing file, status: ", res
!// Stop the program
      STOP
    END IF

  END SUBROUTINE series_print_init

!====================================================================
  SUBROUTINE series_finalize(uid)
    USE parkind_mod
    integer, intent(in)    :: uid
   
    CLOSE(UNIT=uid)
  
  END SUBROUTINE series_finalize
END MODULE series_mod
