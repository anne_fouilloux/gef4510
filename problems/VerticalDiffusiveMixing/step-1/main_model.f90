PROGRAM MAIN_MODEL
 USE parkind_mod
 USE constants_mod
 USE log_mod
 USE model_mod

 IMPLICIT NONE

 type(ModelType)                :: M   ! Model state variable

 character(len=100)             :: namelistFile
 character(len=100)             :: logfile
 integer                        :: i 
 real(kind=double)              :: time

 integer                        :: iargc        ! number of command line arguments
 integer                        :: print_iter

 type(log_t)                    :: log


 ! retrieve the number of command line arguments
 iargc = command_argument_count()

 if (iargc < 1 .or. iargc > 2) then
 ! if not enough arguments, print usage and STOP program
   call print_usage
   STOP
 endif

  !// Retrieve the first argument and put the contents into
  !// the farenheit_file variable
 CALL GET_COMMAND_ARGUMENT(1,namelistFile)

 if (iargc>1) then
   CALL GET_COMMAND_ARGUMENT(2,logfile)
 else
   logfile='log.txt'
 endif

 call log_init(log, logfile=logfile, lunit=21)
 call log_message(log,"Start program")
 call log_message(log,"Read model namelist to get input parameters "// &
                  & namelistFile)
 call model_parameters(M,namelistFile, log)
 call log_message(log,"Set initial conditions")
 call initial_conditions(M)
! write model parameters in logfile
 call print_parameters(M)

! print initial value if required at iteration 0
 print_iter=1
 if (do_print(M,0)) then
     call log_message(log,"Print initial conditions in " // TRIM(M%filename))
     call save_theta(M,print_iter)
     print_iter = print_iter + 1
 endif
! main iteration loop
 call log_message(log, "Start main iteration loop")
 do i=1, N
   time = i * M%dt
   call compute_next_temperature(M,time)
   if (do_print(M,i)) then
     call log_message(log,"Print model outputs")
     call save_theta(M,print_iter)
     print_iter = print_iter + 1
   endif
 enddo

! Free model variables if needed and close output file
 call model_finalize(M)
 call log_message(log,"Program SUCCESSFUL!!!")
 call log_close(log)
!=================================================================
! SUBROUTINES FOR MAIN PROGRAM
!=================================================================
contains
!-------------------------------------------------------------
SUBROUTINE print_usage
  print*, 'USAGE: main.x namelist.in [logfile.txt]'
END SUBROUTINE print_usage
!-------------------------------------------------------------
END PROGRAM MAIN_MODEL
