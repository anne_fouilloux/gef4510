MODULE constants_mod
  USE parkind_mod

  IMPLICIT NONE

  integer, parameter             :: Nindices =   2        ! number of indices kept (current
                                                          ! and next time)

  integer, parameter             :: N       = 1201        ! Number of iterations
  integer, parameter             :: Jmax    =   27        ! total number of grid points
  integer, parameter             :: D       =  100        ! depth of the mixed
                                                          ! layers
  real(kind=double), parameter   :: PI      = 4.*ATAN(1.) ! Mathematical constant = 3.1415...       

! constants for Atmosphere application
  integer, parameter             :: Atmosphere = 1        ! Atmosphere
  real(kind=double), parameter   :: Ka      =   30.d0     ! Atmosphere: Mixing coefficient

! constants for Ocean application
  integer, parameter             :: Ocean = 2             ! Ocean
  real(kind=double), parameter   :: Ko      =   3.0d-03   ! Ocean: Mixing coefficient

  real(kind=double), parameter   :: theta0  =   10.0d0    ! Maximum temperature at the boundary for 
                                                          ! forced case and maximum temperature 
                                                          ! anomaly for unforced case (unit oC)
  real(kind=double),parameter    :: gamma=1.5d0           ! Dimensionless constant used to determine 
                                                          ! how fast (in time) the hyperbolic tangent
                                                          ! approaches the maximum value.
  real(kind=double), parameter   :: Tc= 6.0d0 * 24.0d0 * 3600.0d0 ! number of
                                                          ! seconds in 6 days

! constants for printouts
  integer, parameter             :: Nprintouts = 5        ! number of printouts

contains

subroutine print_constants
  implicit none

  print*, 'PI = ', PI
  print*, 'Number of iterations : ', N
  print*, 'Total number of grid points : ', Jmax
  print*, 'Depth of the mixed layers: ', D
  print*, 'Ka = ', Ka
  print*, 'Ko = ', Ko, ' Gamma = ', gamma
  print*, 'theta0= ', theta0
end subroutine print_constants
END MODULE constants_mod
