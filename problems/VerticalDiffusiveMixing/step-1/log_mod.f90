MODULE log_mod  
  IMPLICIT NONE

  TYPE log_t
    integer                        :: uid_log = 21  ! logical unit number for the log file
    character(len=255)             :: filename="log.txt"
  END TYPE log_t

contains

SUBROUTINE log_message(log, message)
  implicit none
  TYPE(log_t),      intent(inout) :: log
  character(LEN=*), intent(in)    :: message

  logical                         :: is_opened

  INQUIRE (UNIT=log%uid_log, OPENED=is_opened)
  if (.NOT. is_opened) call log_init(log)

  write(UNIT=log%uid_log,FMT=*) " Log - ", message

END SUBROUTINE log_message
!------------------------------------------------------------------

SUBROUTINE check_log_error(res, message)
  implicit none
  integer, intent(in)          :: res
  character(LEN=*), intent(in) :: message

    IF(res /= 0) THEN                          ! An error has occured
      PRINT *, 'Error : ', message,' status: ', res
      STOP                                     ! Stop the program
    END IF     
END SUBROUTINE check_log_error

!------------------------------------------------------------------

SUBROUTINE log_init(log, logfile, lunit)
    implicit none
    
    TYPE(log_t), intent(inout)             :: log
    character(LEN=*), optional, intent(in) :: logfile
    integer, optional, intent(in)          :: lunit
    
    integer                      :: res 
    
    if (PRESENT(lunit)) then
       log%uid_log = lunit
    endif
    if (PRESENT(logfile)) then
       log%filename = logfile
    endif
    OPEN(UNIT=log%uid_log,FILE=log%filename,FORM="FORMATTED",IOSTAT=res)
    call check_log_error(res,"opening log file")
END SUBROUTINE log_init

!------------------------------------------------------------------

SUBROUTINE log_close(log)
  USE parkind_mod
  USE constants_mod
  implicit none
  
  TYPE(log_t), intent(in)      :: log
  integer                      :: res 

  close(UNIT=log%uid_log, IOSTAT=res)
  call check_log_error(res,"close log file")
  
END SUBROUTINE log_close
END MODULE log_mod
