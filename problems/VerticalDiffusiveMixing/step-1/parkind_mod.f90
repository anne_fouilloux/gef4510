MODULE parkind_mod
! here we define single and double real precision 
  integer, parameter   :: single=SELECTED_REAL_KIND(6,37)
!  integer, parameter   :: double=SELECTED_REAL_KIND(15,307)
  integer, parameter   :: double=SELECTED_REAL_KIND(6,37)

 contains

 SUBROUTINE print_parkind
   implicit none
  
   print*, 'Size of a real single-precision: ', single 
   print*, 'Size of a real double-precision: ', double 
 END SUBROUTINE print_parkind
END MODULE parkind_mod
