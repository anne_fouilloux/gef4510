! This program tests all the available functionalities of parkind_mod
! i.e. we test all the available functions/subroutines and print all
! available constants parameters
program test_parkind
  USE parkind_mod

  call print_parkind
end program test_parkind
