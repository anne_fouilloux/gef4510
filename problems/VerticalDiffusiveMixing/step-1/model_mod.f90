MODULE MODEL_MOD
  USE parkind_mod
  USE constants_mod
  USE log_mod
  IMPLICIT NONE

! Definition of Fortran 90 derived types
 TYPE ModelType
! model configuration parameters
   integer                                        :: uid_out = 20 ! logical unit
                                                                ! when writing
                                                                ! output results
   character(len=255)                             :: filename ! to store model outputs
   integer                                        :: type      ! Atmosphere or ocean
   
   logical                                        :: linear    ! linear (=true) case or  
                                                             ! a tanh case (=false) for ocean
   integer, dimension(Nprintouts)                 :: printouts ! indices to know when to write results
   real(kind=double)                              :: K         ! constant used in equation
   real(kind=double)                              :: kappa     ! Mixing coefficient
! model variables 
   real(kind=double), dimension(JMAX)             :: z         ! Depth (or height) coordinate (unit m)
   REAL(kind=double), dimension(JMAX, Nindices)   :: theta     ! Temperature variable (deg C) 
   REAL(kind=double), dimension(JMAX, Nprintouts) :: thetaSave ! Save Temperature variable at different stages for printout (deg C) 
   integer                                        :: icur      ! Index to store current temperature
   integer                                        :: inext     ! Index to store the next temperature
   real(kind=double)                              :: dz        ! space increment
   real(kind=double)                              :: dt        ! time increment

   type(log_t)                                    :: log       ! log model info
 END TYPE ModelType

contains
!
SUBROUTINE model_parameters(t,namelistFile, log)

 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType), intent(out) :: t
 type(log_t)    , intent(in)  :: log

 character(LEN=100)             :: namelistFile ! namelist where all program parameters will be set
 integer                        :: ios         ! check errors

 character(len=1)               :: Case        ! atmosphere or ocean
 logical                        :: linear      ! can be true or false for ocean application only

 character(len=100)             :: outputfile
 real(kind=double)              :: K           
 integer, dimension(Nprintouts) :: printouts
 integer                        :: uid_out

 NAMELIST/MODEL_INPUT/ Case,K,linear, printouts,outputfile,uid_out

 t%log = log

! read namelist
  open( unit=1,                &
        file=namelistFile,     &
        form="formatted",      &
        iostat=ios )
  if (ios /= 0) then
    print*, 'Cannot open namelist input file ', trim(namelistfile)
    STOP
  endif
! read a namelist
  read ( unit=1, nml=MODEL_INPUT )
!
  close( unit=1 )

  t%printouts = printouts
  call log_message(t%log,"Open file " // TRIM(outputfile))
  t%filename = outputfile
  t%uid_out = uid_out

  call file_open(t)

  t%K = K
  if (Case == "A" .or. Case == "a") then
    t%type = Atmosphere
  else
! set ocean if not atmosphere
    t%type = Ocean
    t%linear = linear
  endif
END SUBROUTINE model_parameters
!------------------------------------------------------------------
SUBROUTINE initial_conditions(t)

 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType), intent(inout) :: t

 real(kind=double)            :: dz
 integer                      :: j

! initializations
 
!
 t%icur = 1
 t%inext=2
 t%theta(:,t%inext) = 0.0d0

 t%dz = D/(Jmax-1.0d0)

  select case (t%type)
    case(Atmosphere)      
      t%kappa = Ka     
      t%dt = t%K*t%dz*t%dz/Ka
      do j=1,Jmax
        t%z(j) = (j-1) * t%dz
        t%theta(j,t%icur) = theta0 * sin(PI*t%Z(j)/D)  
      enddo     
    case(Ocean)
      t%kappa = Ko
      t%dt = t%K*t%dz*t%dz/Ko
      do j=1,Jmax
        t%z(j) = - D + (j-1) * t%dz
        t%theta(j,t%icur) = 0.0d0
      enddo
    case default
       print*, 'Error case unknown: choose 1 for Atmosphere and 2 for Ocean'
       STOP
  end select 
 
END SUBROUTINE initial_conditions

!------------------------------------------------------------------
SUBROUTINE boundary_conditions(t,time)

 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType),   intent(inout) :: t
 real(kind=double), intent(in)    :: time  ! iteration time

! compute new temperature at boundaries i.e. 1 and Jmax

  t%theta(1,t%inext) = 0.0
  select case (t%type)
    case(Atmosphere)  
       t%theta(Jmax,t%inext) = 0.0
    case(Ocean)
       if (t%linear) then
          if (time < Tc) then
            t%theta(Jmax,t%inext) = theta0 * time / Tc
          else
            t%theta(Jmax,t%inext) = theta0
          endif
       else
! use hyperbolic tangent
         t%theta(Jmax,t%inext) = theta0 * TANH(gamma*time/Tc)
       endif        
    case default
       print*, 'Error case unknown: choose 1 for Atmosphere and 2 for Ocean'
       STOP
  end select 

END SUBROUTINE boundary_conditions


!------------------------------------------------------------------
SUBROUTINE prepare_next_iteration(t)

 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType), intent(inout) :: t
 integer                        :: isave

 isave = t%icur
 t%icur = t%inext
 t%inext = isave
END SUBROUTINE prepare_next_iteration

!------------------------------------------------------------------
SUBROUTINE compute_next_temperature(t, time)

 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType),   intent(inout) :: t
 real(kind=double), intent(in)    :: time  ! iteration time

 integer                          :: j


! Now compute next values except at boundaries
  do j=2, Jmax-1 
    t%theta(j,t%inext) = t%theta(j,t%icur) + &
                         t%K*(t%theta(j-1,t%icur)-2.*t%theta(j,t%icur)+t%theta(j+1,t%icur))
  enddo 

! compute next temperature at boundaries
  call boundary_conditions(t,time)
  call prepare_next_iteration(t)
END SUBROUTINE compute_next_temperature

!------------------------------------------------------------------
SUBROUTINE model_finalize(t)
  
 USE parkind_mod
 USE constants_mod

 implicit none

  type(ModelType),   intent(in) :: t
  integer  :: res
  integer  :: level

  do level=1,JMAX
! print in an outputfile
     write(UNIT=t%uid_out,FMT='(1X,F10.4,2X,5F12.4)') t%z(level)/D,t%thetaSave(level,:)
  enddo
  close(UNIT=t%uid_out, IOSTAT=res)

  IF (res /= 0) THEN
    PRINT*, 'Error in closing outputfile '
    STOP
  ENDIF 
END SUBROUTINE model_finalize
!------------------------------------------------------------
SUBROUTINE file_open(t)
  USE parkind_mod
  USE constants_mod
  IMPLICIT NONE

  type(ModelType),   intent(in) :: t

  integer                      :: res

 OPEN(UNIT=t%uid_out, FILE=t%filename, FORM="formatted", IOSTAT=res)
 IF (res /= 0) THEN
   PRINT*, 'Error in opening outputfile : ', trim(t%filename)
   STOP
 ENDIF
 
 
END SUBROUTINE file_open

!------------------------------------------------------------------
SUBROUTINE save_theta(t,iter)
 USE parkind_mod
 USE constants_mod

 implicit none

 type(ModelType), intent(inout):: t
 integer,         intent(in)   :: iter
 integer                       :: level

 do level=1,JMAX
! save for later
   t%thetaSave(level, iter) = t%theta(level,t%icur)/theta0
 enddo
END SUBROUTINE save_theta
!----------------------------------------------------------
FUNCTION do_print(t,iter)
 USE parkind_mod
 USE constants_mod
 implicit none
 type(ModelType), intent(in)       :: t
 integer,               intent(in) :: iter
 logical                           :: do_print

 integer                           :: i

! check whether we need to print output results
 do_print=.false.
 do i=1,size(t%printouts)
  if (t%printouts(i) == iter) then
    do_print = .true.
    exit
  endif
 enddo
END FUNCTION do_print
!------------------------------------------------------------------
SUBROUTINE print_parameters(t)
  USE parkind_mod
  USE constants_mod
  implicit none

  type(ModelType), intent(in)   :: t

  integer                       :: lu

  lu = t%log%uid_log

  select case (t%type)
    case(Atmosphere)  
       write(UNIT=lu,FMT=*) 'Atmosphere application'
    case(Ocean)
        write(UNIT=lu,FMT=*) 'Ocean application'
    case default
       write(UNIT=lu,FMT=*) 'Error case unknown: choose 1 for Atmosphere and 2 for Ocean'
       STOP
  end select 
  
  write(UNIT=lu,FMT=*) 'K = ', t%K
  write(UNIT=lu,FMT=*) 'kappa = ', t%kappa
  write(UNIT=lu,FMT=*) 'outputs will be written at ', t%printouts
  if (t%type==Ocean) then
    if (t%linear) then
      write(UNIT=lu,FMT=*) 'Linear boundary conditions'
    else
     write(UNIT=lu,FMT=*) 'Hyperbolic tangent boundary conditions'
    endif
  endif

 write(UNIT=lu,FMT=*) 'Number of grid points: ', Jmax
 write(UNIT=lu,FMT=*) 'Number of iterations: ', N
 write(UNIT=lu,FMT=*) 'Depth of the mixed layers: ', D
 write(UNIT=lu,FMT=*) 'gamma = ', gamma
 write(UNIT=lu,FMT=*) 'theta0 = ', theta0
 write(UNIT=lu,FMT=*) 'D = ', D
 write(UNIT=lu,FMT=*) 'Jmax-1 = ', Jmax-1
 write(UNIT=lu,FMT=*) 'Tc = ', Tc, ' seconds'
 write(UNIT=lu,FMT=*) 'dz = ', t%dz
 write(UNIT=lu,FMT=*) 'dt = ', t%dt, 's'
END SUBROUTINE print_parameters
!------------------------------------------------------------------
END MODULE MODEL_MOD
